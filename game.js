//Game - designed by Cormac White and Kieran Hoey
var Game = function ()
{
this.canvas = document.getElementById('game-canvas'),
this.context = this.canvas.getContext('2d'),
this.fpsElement = document.getElementById('fps'),
this.scoreElement = document.getElementById('score'),
this.livesElement = document.getElementById('lives'),
this.livesIDElement = document.getElementById('livesID'),
this.levelElement = document.getElementById('level'),
this.canvasWidth = this.canvas.width,
this.canvasHeight = this.canvas.height,

//mobile
this.mobileInstructionsVisible = false,
this.mobileStartToast = document.getElementById('game-mobile-start-toast'),
this.mobileWelcomeToast = document.getElementById('game-mobile-welcome-toast'),
this.welcomeStartLink = document.getElementById('game-welcome-start-link'),
this.showHowLink = document.getElementById('game-show-how-link'),
this.mobileStartLink = document.getElementById('game-mobile-start-link'),

this.lives = 5,
this.score = 0,
this.level = 0,
this.HERO_EXPLOSION_DURATION = 500,
//Loading
this.loadingElement = document.getElementById('loading'),
//Toast
this.toastElement = document.getElementById('toast'),
//Instructions
this.instructionsElement = document.getElementById('instructions'),
//Copyright
this.copyrightElement = document.getElementById('copyright'),

//Sound and music
this.soundAndMusicElement = document.getElementById('sound-and-music'),
this.musicElement = document.getElementById('game-music'),
this.musicElement.volume = 0.1,
this.musicCheckboxElement = document.getElementById('music-checkbox'),
this.soundCheckboxElement = document.getElementById('sound-checkbox'),
this.musicOn = this.musicCheckboxElement.checked,
this.audioSprites = document.getElementById('game-audio-sprites'),
this.soundOn = this.soundCheckboxElement.checked,

this.audioChannels = [
  {playing: false, audio: this.audioSprites,},
  {playing: false, audio: null,},
  {playing: false, audio: null,},
  {playing: false, audio: null}],
//Sounds
this.audioSpriteCountdown = this.audioChannels.length - 1,
this.graphicsReady = false,

//Sounds
   this.explosionSound = 
   {
      position: 4.3,
      duration: 760,
      volume: 0.3
   },

   this.pianoSound = 
   {
      position: 5.6,
      duration: 395,
      volume: 0.2
   },

   this.coinSound = 
   {
      position: 7.1,
      duration: 588,
      volume: 0.2
   },



//Levels
this.difficulty = 10,
this.scrollMultiplier = 1,
//Background
this.backgroundSpritesheet = new Image(),
this.BACKGROUND_VELOCITY = 70,
this.STARTING_BACKGROUND_VELOCITY_X = 0,
this.STARTING_BACKGROUND_VELOCITY_Y = -20*this.scrollMultiplier,
this.STARTING_BACKGROUND_OFFSET = 0,
this.BACKGROUND_HEIGHT = 880,
this.BACKGROUND_WIDTH = 240,
this.backgroundOffsetX = this.STARTING_BACKGROUND_OFFSET,
this.backgroundOffsetY = this.STARTING_BACKGROUND_OFFSET,
this.bgVelocityX = this.STARTING_BACKGROUND_VELOCITY_X,
this.bgVelocityY = this.STARTING_BACKGROUND_VELOCITY_Y,
//Player Changes
this.PLAYER_VELOCITY = 84,
this.STARTING_PLAYER_VELOCITY = 0,
this.pVelocityY = 0,
this.pVelocityX = 0,
this.playerOffsetY = 0,
this.playerOffsetX = 0,
this.RUN_ANIMATION_RATE = 10,
//Sprite Changes
this.spriteOffsetY = 0,
this.spriteOffsetX = 0,
this.spriteVelocityX = 0,
this.spriteVelocityY = 0,
this.gameStarted = false,
//Time
this.lastAnimationFrameTime = 0,
this.fps = 60,
this.lastFpsUpdateTime = 0,
this.lastAdvanceTime = 0,
this.pausedTime,
//States
this.paused = false,
this.CHECK_PAUSED = 200,
this.focusWindow = true,
this.currentCountdown = false,
//Constants
this.MINI_DELAY = 50,
this.TRANSPARENT = 0,
this.OPAQUE = 1.0,
this.timeRate = 1.0,
this.timeSystem = new TimeSystem(),
//Images
this.heroSpritesheet = new Image(),
this.backgroundSpritesheet = new Image(),
this.likeLikeSpritesheet = new Image(),
this.rupeeSpritesheet = new Image(),
//Directions
this.LEFT = 1,
this.RIGHT = 2,
this.UP = 3,
this.DOWN = 4;

//EXPLOSIONS
this.EXPLOSION_CELLS_HEIGHT = 51,

this.explosionCells = [
      { left: -11,   top: 293, 
        width: 31, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 34,   top: 293, 
        width: 47, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 84,  top: 293, 
        width: 36, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 120, top: 293, 
        width: 30, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 150, top: 293, 
        width: 32, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 184, top: 293, 
        width: 35, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 223, top: 293, 
        width: 38, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 263, top: 293, 
        width: 39, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 305, top: 293, 
        width: 38, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 341, top: 293, 
        width: 33, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 371, top: 293, 
        width: 30, height: this.EXPLOSION_CELLS_HEIGHT }
   ];




this.heroCellsFront = [
{left: 2, top: 3, width: 23, height: 30},
{left: 32, top: 3, width: 23, height: 30},
{left: 62, top: 3, width: 23, height: 30}
];

this.heroCellsLeft = [
{left: 5, top: 37, width: 22, height: 28},
{left: 35, top: 37, width: 22, height: 28},
{left: 259, top: 149, width: 86, height: 111}
];

this.heroCellsBack = [
{left: 3, top: 71, width: 24, height: 27}
];

this.heroCellsRight = [
{left: 3, top: 102, width: 22, height: 28},
{left: 32, top: 102, width: 22, height: 28},
{left: 63, top: 102, width: 22, height: 28}
];

this.heroCellsFrontWalk = [
{left: 3, top: 133, width: 24, height: 30},
{left: 33, top: 132, width: 24, height: 28},
{left: 63, top: 132, width: 24, height: 31},
{left: 93, top: 132, width: 24, height: 31},
{left: 123, top: 132, width: 24, height: 31},
{left: 153, top: 133, width: 24, height: 30},
{left: 183, top: 132, width: 24, height: 28},
{left: 213, top: 132, width: 24, height: 31},
{left: 243, top: 132, width: 24, height: 31},
{left: 273, top: 132, width: 24, height: 31}
];

this.heroCellsLeftWalk = [
{left: 1, top: 167, width: 29, height: 28},
{left: 32, top: 167, width: 25, height: 27},
{left: 61, top: 166, width: 24, height: 26},
{left: 92, top: 166, width: 25, height: 28},
{left: 122, top: 166, width: 26, height: 28},
{left: 151, top: 167, width: 29, height: 28},
{left: 182, top: 167, width: 25, height: 28},
{left: 212, top: 166, width: 23, height: 27},
{left: 243, top: 166, width: 25, height: 27},
{left: 272, top: 166, width: 26, height: 28}
];

this.heroCellsBackWalk = [
{left: 3, top: 197, width: 24, height: 28},
{left: 33, top: 197, width: 24, height: 28},
{left: 63, top: 195, width: 24, height: 30},
{left: 93, top: 195, width: 24, height: 30},
{left: 123, top: 195, width: 24, height: 30},
{left: 153, top: 197, width: 24, height: 28},
{left: 183, top: 197, width: 24, height: 28},
{left: 213, top: 195, width: 24, height: 30},
{left: 243, top: 196, width: 24, height: 31},
{left: 273, top: 195, width: 24, height: 30}
];

this.heroCellsRightWalk = [
{left: 1, top: 231, width: 26, height: 28},
{left: 33, top: 231, width: 25, height: 28},
{left: 63, top: 231, width: 25, height: 28},
{left: 92, top: 232, width: 25, height: 28},
{left: 120, top: 232, width: 29, height: 28},
{left: 152, top: 231, width: 26, height: 28},
{left: 182, top: 231, width: 25, height: 28},
{left: 215, top: 231, width: 24, height: 26},
{left: 245, top: 232, width: 24, height: 27},
{left: 270, top: 232, width: 29, height: 28}
];

this.likeLikeCells = [
//small to big
{left: -2, top: 2, width: 12, height: 23},
{left: 11, top: 2, width: 16, height: 23},
{left: 28, top: 2, width: 16, height: 23},
{left: 45, top: 2, width: 16, height: 23},
//bounce 1
{left: 62, top: 2, width: 16, height: 23},
//bounce 2
{left: 79, top: 2, width: 16, height: 23},
{left: 97, top: 2, width: 16, height: 23},
{left: 114, top: 2, width: 16, height: 23},
{left: 131, top: 2, width: 16, height: 23},
{left: 148, top: 2, width: 16, height: 23},
//bounce 3
{left: 165, top: 2, width: 16, height: 23},
//bounce 4
{left: 148, top: 2, width: 16, height: 23},
{left: 131, top: 2, width: 16, height: 23},
{left: 114, top: 2, width: 16, height: 23},
{left: 97, top: 2, width: 16, height: 23},
{left: 79, top: 2, width: 16, height: 23},
//bounce 5
{left: 62, top: 2, width: 16, height: 23},
//bounce 6
{left: 79, top: 2, width: 16, height: 23},
{left: 97, top: 2, width: 16, height: 23},
{left: 114, top: 2, width: 16, height: 23},
{left: 131, top: 2, width: 16, height: 23},
{left: 148, top: 2, width: 16, height: 23},
//bounce 7
{left: 165, top: 2, width: 16, height: 23},
//bounce 8
{left: 148, top: 2, width: 16, height: 23},
{left: 131, top: 2, width: 16, height: 23},
{left: 114, top: 2, width: 16, height: 23},
{left: 97, top: 2, width: 16, height: 23},
{left: 79, top: 2, width: 16, height: 23},
//bounce 9
{left: 62, top: 2, width: 16, height: 23},
//big to small
{left: 45, top: 2, width: 16, height: 23},
{left: 28, top: 2, width: 16, height: 23},
{left: 11, top: 2, width: 16, height: 23}
];

this.greenRupeeCells = [
{left: 41, top: 1, width: 19, height: 34},
{left: 62, top: 1, width: 19, height: 34},
{left: 83, top: 1, width: 19, height: 34}
];

this.blueRupeeCells = [
{left: 41, top: 35, width: 19, height: 34},
{left: 62, top: 35, width: 19, height: 34},
{left: 83, top: 35, width: 19, height: 34}
];

this.acornCells = [
{left: 123, top: 915, width: 74, height: 59}];

this.likeLikeData = [
{left: 100, top: 250}
];
this.leafCells = [{left: 8, top: 906, width: 93, height: 68}];

this.leafData = [];
this.likeLikes = [];
this.leafs = [];
this.rupees = [];
this.acorns = [];
this.sprites = [];

//Sprite behaviours
this.runBehaviour =
{
  lastAdvanceTime: 0,
  execute: function (sprite, now, fps, context,
          lastAnimationFrameTime)
  {
      if (sprite.runAnimationRate === 0)
      {
          return;
      }
      if (this.lastAdvanceTime === 0)
      {
          this.lastAdvanceTime = now;
      }
      else if (now - this.lastAdvanceTime >
              1000 / sprite.runAnimationRate)
      {
          sprite.artist.advance();
          this.lastAdvanceTime = now;
      }
  }
};

//Collision Detection
this.collideBehaviour=
   {
      isCandidateForCollision: function(sprite, otherSprite)
      {
         var s, o;
         s = sprite.calculateCollisionRectangle(),
         o = otherSprite.calculateCollisionRectangle();
         return o.left < s.right && sprite !== otherSprite &&
               sprite.visible && otherSprite.visible && !sprite.exploding && 
               !otherSprite.exploding;
      },

      didCollide: function(sprite, otherSprite, context)
      {
         var s, o;
         s = sprite.calculateCollisionRectangle(),
         o = otherSprite.calculateCollisionRectangle();
         //Check whether any of the hero's four corners or centre
         //lie in the other sprites bounding box
         context.beginPath();
         context.rect(o.left, o.top, o.right-o.left, o.bottom-o.top);
         return context.isPointInPath(s.left, s.top) ||
                context.isPointInPath(s.right, s.top) ||
                context.isPointInPath(s.centreX, s.centreY) ||
                context.isPointInPath(s.left, s.bottom) ||
                context.isPointInPath(s.right, s.bottom);
      },

      processBadGuyCollision: function(sprite, otherSprite)
      {
        Game.playSound(Game.explosionSound);
        Game.explode(sprite);
        if(sprite.type === 'heroSprite')
        {
          Game.lives -= 1;
          Game.livesElement.innerHTML = Game.lives;
          if(Game.livesElement.innerHTML > 0)
          {
            Game.loseLife();
          }
          else
          {
            Game.endGame();
          }
        }    
      },

      processCollision: function(sprite, otherSprite)
      {
         if('rupeeblue' === otherSprite.type || 'rupeegreen' === otherSprite.type)
         {
            otherSprite.visible = false;
            if('rupeeblue' === otherSprite.type)
            {
              Game.score += 10;
              Game.scoreElement.innerHTML = "$="+Game.score;
              Game.playSound(Game.pianoSound);
            }
            else if('rupeegreen' === otherSprite.type)
            {
              Game.score += 5;
              Game.scoreElement.innerHTML = "$="+Game.score;
              Game.playSound(Game.pianoSound);
            }
         }
         if('likeLike' === otherSprite.type)
         {
            this.processBadGuyCollision(sprite, otherSprite);
         }
         if('acorn' === otherSprite.type)
         {
          Game.score += 15;
          Game.level++;
          Game.difficulty += 2;
          Game.scrollMultiplier +=0.5; 
          Game.scoreElement.innerHTML = "$="+Game.score;
          Game.levelElement.innerHTML = "LvL="+Game.level;
          Game.playSound(Game.coinSound);
          Game.reset();

         }
      },
      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
         var otherSprite;
         for(var i=0; i < Game.sprites.length; ++i)
         {
            otherSprite = Game.sprites[i];
            if(this.isCandidateForCollision(sprite, otherSprite))
            {
               if(this.didCollide(sprite, otherSprite, context))
               {

                  this.processCollision(sprite, otherSprite);
               }
            }
         }
      }
   };

this.paceBehaviour = 
   {
      setDirection: function(sprite)
      {
         var sRight = sprite.left + sprite.width,
             pRight = this.canvasWidth;

         if(sprite.direction === undefined)
         {
            sprite.direction = Game.RIGHT;
         }
         if(sRight > pRight && sprite.direction === Game.RIGHT)
         {
            sprite.direction = Game.LEFT;
         }
         else if(sprite.left < this.canvasWidth && sprite.direction === Game.LEFT)
         {
            sprite.direction = Game.RIGHT;
         }
      },

      setPosition: function(sprite, now, lastAnimationFrameTime)
      {
         var pixelsToMove = sprite.velocityX * 
                              (now-lastAnimationFrameTime)/1000;
         if(sprite.direction === Game.RIGHT)
         {
            sprite.left += pixelsToMove;
         }                    
         else
         {
            sprite.left -= pixelsToMove;
         }

      },
      execute: function(sprite, now, fps,context, 
         lastAnimationFrameTime)
      {
         this.setDirection(sprite);
         this.setPosition(sprite, now, lastAnimationFrameTime);
      }
   };

this.heroExplodeBehaviour = new CellSwitchBehaviour(
      this.explosionCells,
      this.HERO_EXPLOSION_DURATION,

      function (sprite, now, fps) { // Trigger
         return sprite.exploding;
      },
                                                
      function (sprite, animator) { // Callback
         sprite.exploding = false;
      }
   );
};

//Launching the game
Game.prototype =
{
  explode: function (sprite) {
      if ( ! sprite.exploding) {
         if (sprite.runAnimationRate === 0) {
            sprite.runAnimationRate = this.RUN_ANIMATION_RATE;
         }

         sprite.exploding = true;
      }
   },
  endGame: function()
  {
    var TOAST_GAP = 1000,
        TOAST_DURATION = 2500;

        Game.paused = true;

    setTimeout(function ()
    {
      Game.toastDisplay('Nice Try! You\'ll be refreshed shortly!', TOAST_GAP);
    }, TOAST_DURATION);
    window.setTimeout(function(){
    window.location.reload();
    }, 6000);
  },
  startGame: function ()
  {
      this.gameReveal();
      if(Game.mobile)
      {
         //this.elementFadeIn(Game.mobileWelcomeToast);
      }
      else
      {   
        this.welcomeToastReveal();
        this.playing = true;
      }
      this.startMusic();
      this.timeSystem.start();
      this.setTimeRate(1.0);
      this.gameStarted = true;
      window.requestAnimationFrame(this.animate);
  },
  createSprites: function ()
  {
      this.createHeroSprite(),
      this.createLeafSprites(),
      this.createRupeeSprites(),
      this.createlikeLikeSprites(),
      this.createAcornSprite(),
      this.initSprites();
      this.spriteArrayInit();
  },
  initSprites: function ()
  {
      this.positionSprites(this.likeLikes, this.likeLikeData);
      this.positionSprites(this.leafs, this.leafData);
      this.positionSprites(this.rupees, this.rupeeCells);
      this.positionSprites(this.acorns, this.acornCells);
  },
  createHeroSprite: function ()
  {
      var   HERO_HEIGHT = 30,
            HERO_WIDTH = 23,
            START_ANIMATION_RATE = 0;

      this.hero = new Sprite('heroSprite', new SpriteSheetArtist(
              this.heroSpritesheet, this.heroCellsBackWalk), [this.runBehaviour, this.collideBehaviour,this.heroExplodeBehaviour]);
      this.hero.runAnimationRate = START_ANIMATION_RATE;
      this.hero.left = (this.canvasWidth / 2) - (HERO_WIDTH / 2);
      this.hero.height = HERO_HEIGHT;
      this.hero.top = this.canvasHeight;
      console.log("Left:"+this.hero.left);
      console.log("Top:"+this.hero.height);
      this.sprites.push(this.hero);
  },
  positionSprites: function (sprites, spriteData) {
      var sprite;

      for (var i = 0; i < sprites.length; ++i) {
          sprite = sprites[i];
        sprite.top = spriteData[i].top;
        sprite.left = spriteData[i].left;
      }
  },
  createlikeLikeSprites: function ()
  {
      var likeLike,
      LIKE_LIKE_WIDTH = 16,
      LIKE_LIKE_HEIGHT= 23,
              likeLikeChange = 100,
              likeLikeInterval = 150;

      for (var i = 0; i < this.difficulty; ++i)
      {
          likeLike = new Sprite('likeLike', new SpriteSheetArtist(
                  this.likeLikeSpritesheet, this.likeLikeCells), [new CycleBehaviour(likeLikeChange, likeLikeInterval), this.paceBehaviour]);
          likeLike.width = LIKE_LIKE_WIDTH;
          likeLike.height = LIKE_LIKE_HEIGHT;
          likeLike.left = Math.floor(Math.random() * (this.canvasWidth - likeLike.width) + 0);
          likeLike.top = Math.floor(Math.random() * (700)) - this.canvasHeight + likeLike.height;
          this.sprites.push(likeLike);
      }
  },
  createLeafSprites: function ()
  {
      var leaf;

      for (var i = 0; i < 3; ++i)
      {
          leaf = new Sprite('leaf', new SpriteSheetArtist(
                  this.backgroundSpritesheet, this.leafCells));
          leaf.left = Math.floor(Math.random() * (this.canvasWidth - leaf.width) + 0);
          leaf.top = Math.floor(Math.random() * (700)) - this.canvasHeight + leaf.height;
          this.sprites.push(leaf);
      }
  },

  createAcornSprite: function ()
  {
      var acorn,
          ACORN_WIDTH = 60,
          ACORN_HEIGHT = 30;

      for (var i = 0; i < 1; ++i)
      {
          acorn = new Sprite('acorn', new SpriteSheetArtist(
                  this.backgroundSpritesheet, this.acornCells));
          acorn.width = ACORN_WIDTH;
          acorn.height = ACORN_HEIGHT;
          acorn.left = 80;
          acorn.top = -450;
          this.sprites.push(acorn);
      }
  },

  createRupeeSprites: function ()
  {
      var rupee,
          rupeeChange = 100,
          rupeeInterval = 150;

      for (var i = 0; i < 10; ++i)
      {
        if(i%2 === 0)
        {
          rupee = new Sprite('rupeegreen', new SpriteSheetArtist(
                  this.rupeeSpritesheet, this.greenRupeeCells), [new CycleBehaviour(rupeeChange, rupeeInterval)]);
        }
        else
        {
          rupee = new Sprite('rupeeblue', new SpriteSheetArtist(
                  this.rupeeSpritesheet, this.blueRupeeCells), [new CycleBehaviour(rupeeChange, rupeeInterval)]);
        }


          rupee.left = Math.floor(Math.random() * (this.canvasWidth - rupee.width) + 0);
          rupee.top = Math.floor(Math.random() * (700)) - this.canvasHeight + rupee.height;
          this.sprites.push(rupee);

      }
  },

  spriteArrayInit: function ()
  {
      for (var i = 0; i < this.likeLikes.length; ++i)
      {
          this.sprites.push(this.likeLike[i]);
      }

      this.sprites.push(this.hero);
  },
  setTimeRate: function (rate)
  {
      this.timeRate = rate;
      this.timeSystem.setTransducer(function (now)
      {
          return now * Game.timeRate;
      });
  },
  imagesInit: function ()
  {
      this.heroSpritesheet.src = 'images/banterSpriteSheet.png';
      this.backgroundSpritesheet.src = 'images/background.png';
      this.likeLikeSpritesheet.src = 'images/likeLikeSpritesheet.png';
      this.rupeeSpritesheet.src = 'images/rupeeSpritesheet.png';
      this.heroSpritesheet.onload = function (e)
      {
          Game.imagesLoaded();
      };
  },
  imagesLoaded: function ()
  {
      var WAIT_BEFORE_PLAY = 1500;
      this.graphicsReady = true;
      this.elementFadeOut(this.loadingElement, WAIT_BEFORE_PLAY);

      setTimeout(function () {
          Game.startGame();
          Game.gameStarted = true;
      }, WAIT_BEFORE_PLAY);
  },
  animate: function (now)
  {
      now = Game.timeSystem.calculateGameTime();
      if (Game.paused)
      {
          setTimeout(function ()
          {
              requestAnimationFrame(Game.animate);
          }, Game.CHECK_PAUSED);
      }
      else
      {
          Game.fps = Game.calculateFps(now);
          Game.draw(now);
          Game.lastAnimationFrameTime = now;
          requestAnimationFrame(Game.animate);
      }
  },

  startMusic: function()
   {
      var MUSIC_DELAY = 1000;

      setTimeout(function()
      {
         if(Game.musicCheckboxElement.checked)
         {
            Game.musicElement.play();
         }
         Game.pollMusic();
      }, MUSIC_DELAY);
   },

   pollMusic: function()
   {
      var POLL_INTERVAL = 500,
          SOUNDTRACK_LENGTH = 132,
          timerID;

      timerID = setInterval(function()
      {
         if(Game.musicElement.currentTime > SOUNDTRACK_LENGTH)
         {
            clearInterval(timerID); //stop polling
            Game.restartMusic();
         }
      }, POLL_INTERVAL);
   },

   restartMusic: function()
   {
      Game.musicElement.pause();
      Game.musicElement.currentTime = 0;
      Game.startMusic();
   },

   createAudioChannels: function()
   {
      var channel;

      for(var i=0; i < this.audioChannels.length; ++i)
      {
         channel = this.audioChannels[i];
         if(i !== 0)
         {
            channel.audio = document.createElement('audio');
            channel.audio.addEventListener('loaddata',
               this.soundLoaded, false);
            channel.audio.src = this.audioSprites.currentSrc;
         }
         channel.audio.autobuffer = true;
      }
   },

   soundLoaded: function()
   {
      Game.audioSpriteCountdown--;
      if(Game.audioSpriteCountdown === 0)
      {
         if(!Game.gameStarted && Game.graphicsReady)
         {
            Game.startGame();
         }
      }
   },

   playSound: function(sound)
   {
      var channel,
          audio;

      if(this.soundOn)
      {
         channel = this.getFirstAvailableAudioChannel();

         if(!channel)
         {
            if(console)
            {
               console.warn('All audio channels are busy. Cant play sound');
            }
         }
         else
         {
            audio = channel.audio;
            audio.volume = sound.volume;

            this.seekAudio(sound, audio);
            this.playAudio(audio, channel);

             setTimeout(function () {
               channel.playing = false;
               Game.seekAudio(sound, audio);
            }, sound.duration);
         }
      }
   },

   getFirstAvailableAudioChannel: function()
   {
      for(var i =0; i < this.audioChannels.length; ++i)
      {
         if(!this.audioChannels[i].playing)
         {
            console.log(i);
            return this.audioChannels[i];
         }
      }
      return null;
   },

   seekAudio: function(sound, audio)
   {
      try
      {
         audio.pause();
         audio.currentTime = sound.position;
      }
      catch(e)
      {
         console.error('Cannot play audio');
      }
   },

   playAudio: function(audio, channel)
   {
      try
      {
         audio.play();
         channel.playing = true;
      }
      catch(e)
      {
         if(console)
         {
            console.error('Cannot play audio');
         }
      }
   },



loseLife: function()
   {
      var TRANSITION_DURATION = 3000;


      if(this.hero.exploding)
      {
        this.startLifeTransition(Game.HERO_EXPLOSION_DURATION);
        TRANSITION_DURATION += Game.HERO_EXPLOSION_DURATION;
      }
      else
      {
        this.startLifeTransition();
      }

      setTimeout( function () { // After the explosion
         Game.endLifeTransition();
      }, TRANSITION_DURATION); 
   },


startLifeTransition: function (slowMotionDelay) {
      var CANVAS_TRANSITION_OPACITY = 0.05,
          SLOW_MOTION_RATE = 0.1;

      this.canvas.style.opacity = CANVAS_TRANSITION_OPACITY;
      this.playing = false;

      setTimeout( function () {
         Game.setTimeRate(SLOW_MOTION_RATE);
         Game.hero.visible = false;
      }, slowMotionDelay);
   },

   endLifeTransition: function () {
      var TIME_RESET_DELAY = 1000,
          RUN_DELAY = 500;

      this.reset();

      setTimeout( function () { // Reset the time
         Game.setTimeRate(1.0);

         setTimeout( function () { // Stop running
            Game.hero.runAnimationRate = 0;
            Game.playing = true;
         }, RUN_DELAY);
      }, TIME_RESET_DELAY);
   },
  reset: function () {

      this.sprites = [];
      this.createSprites();
      this.resetOffsets();
      this.resetHero();
      this.makeAllSpritesVisible();
      this.canvas.style.opacity = 1.0;
   },

 resetOffsets: function () {
      this.bgVelocityX        = 0;
      this.bgVelocityY        = 0;
      this.backgroundOffsetX  = 0;
      this.backgroundOffsetY  = 0;
      this.playerOffsetX      = 0;
      this.playerOffsetY      = 0;
      this.spriteOffsetX      = 0;
      this.spriteOffsetY      = 0;
   },

resetHero: function () {
      this.hero.left      = (this.canvasWidth/2)-(this.hero.width/2);
      this.hero.OffsetX   = 0;
      this.hero.OffsetY   = 0;
      this.hero.visible   = true;
      this.hero.exploding = false;
      this.hero.top       = this.canvasHeight-this.hero.height;

      this.hero.artist.cells     = this.heroCellsBack;
      this.hero.artist.cellIndex = 0;
   },

    makeAllSpritesVisible: function () {
      for (var i=0; i < this.sprites.length; ++i) {
         this.sprites[i].visible = true; 
      }
   },

      draw: function (now)
      {
          this.setOffsets(now);
          this.drawBackgroundOne();
          this.drawBackgroundTwo();
          this.updateSprites(now);
          this.drawSprites();
          this.drawBackgroundThree();
          if(Game.mobileInstructionsVisible)
          {
            Game.drawMobileInstructions();
          }
      },
      isSpriteInView: function (sprite)
      {
          //only draw sprite if visible on canvas
          return (sprite.left + sprite.width > sprite.OffsetX && sprite.left < sprite.OffsetX + this.canvas.width)
                  || (sprite.top + sprite.height > sprite.OffsetY && sprite.top < sprite.OffsetY + this.canvas.height);
      },
      updateSprites: function (now)
      {
          var sprite;
          for (var i = 0; i < this.sprites.length; ++i)
          {
              sprite = this.sprites[i];
              if (sprite.visible && this.isSpriteInView(sprite))
              {
                  sprite.update(now, this.fps, this.context,
                          this.lastAnimationFrameTime);
              }
          }
      },
      drawSprites: function ()
      {
          var sprite;
          for (var i = 0; i < this.sprites.length; ++i)
          {
              sprite = this.sprites[i];
              if (sprite.visible && this.isSpriteInView(sprite))
              {
                  this.context.translate(-sprite.OffsetX, -sprite.OffsetY);
                  sprite.draw(this.context);
                  this.context.translate(sprite.OffsetX, sprite.OffsetY);
              }
          }
      },
      calculateFps: function (now)
      {
          var fps = 1 / (now - this.lastAnimationFrameTime) * 1000 * this.timeRate;
          if (now - this.lastFpsUpdateTime > 1000)
          {
              this.lastFpsUpdateTime = now;
              this.fpsElement.innerHTML = fps.toFixed(0) + 'fps';
          }
          return fps;
      },
      welcomeToastReveal: function ()
      {
          var TOAST_GAP = 2000,
                  TOAST_DURATION = 2500;

          setTimeout(function ()
          {
              Game.toastDisplay('Avoid enemies and obstacles! Collect Rupees to score points!', TOAST_GAP);
          }, TOAST_DURATION);
      },
      toastDisplay: function (text, duration)
      {
          var TOAST_GENERAL_LENGTH = 1500;
          duration = duration || TOAST_GENERAL_LENGTH;

          this.commenceToast(text, duration);

          setTimeout(function (e)
          {
              Game.toastAway();

          }, duration);
      },
      drawBackgroundOne: function ()
      {
          var BACKGROUND_1_WIDTH = 240,
                  BACKGROUND_1_HEIGHT = 880,
                  BACKGROUND_1_TOP_IN_SPRITESHEET = 0,
                  BACKGROUND_1_LEFT_IN_SPRITESHEET = 0;
          this.context.translate(-this.backgroundOffsetX, -this.backgroundOffsetY);
          //Initially Onscreen
          this.context.drawImage(this.backgroundSpritesheet, BACKGROUND_1_LEFT_IN_SPRITESHEET,
                  BACKGROUND_1_TOP_IN_SPRITESHEET, BACKGROUND_1_WIDTH, BACKGROUND_1_HEIGHT,
                  0, -(880 - 400), 240,
                  880);

          this.context.translate(this.backgroundOffsetX, this.backgroundOffsetY);
      },
      drawBackgroundTwo: function ()
      {
          var BACKGROUND_2_WIDTH = 240,
                  BACKGROUND_2_HEIGHT = 880,
                  BACKGROUND_2_TOP_IN_SPRITESHEET = 0,
                  BACKGROUND_2_LEFT_IN_SPRITESHEET = 260;
          this.context.translate(-this.backgroundOffsetX, -this.backgroundOffsetY);
          //Initially Onscreen
          this.context.drawImage(this.backgroundSpritesheet, BACKGROUND_2_LEFT_IN_SPRITESHEET,
                  BACKGROUND_2_TOP_IN_SPRITESHEET, BACKGROUND_2_WIDTH, BACKGROUND_2_HEIGHT,
                  0, -(880 - 400), 240,
                  880);

          this.context.translate(this.backgroundOffsetX, this.backgroundOffsetY);
      },
      drawBackgroundThree: function ()
      {
          var BACKGROUND_3_WIDTH = 240,
                  BACKGROUND_3_HEIGHT = 960,
                  BACKGROUND_3_TOP_IN_SPRITESHEET = 0,
                  BACKGROUND_3_LEFT_IN_SPRITESHEET = 520;
          this.context.translate(-this.backgroundOffsetX * (960 / 880), -this.backgroundOffsetY * (960 / 880));
          //Initially Onscreen
          this.context.drawImage(this.backgroundSpritesheet, BACKGROUND_3_LEFT_IN_SPRITESHEET,
                  BACKGROUND_3_TOP_IN_SPRITESHEET, BACKGROUND_3_WIDTH, BACKGROUND_3_HEIGHT,
                  0, -(960 - 400), 240,
                  960);

          this.context.translate(this.backgroundOffsetX * (960 / 880), this.backgroundOffsetY * (960 / 880));
      },
      setBackgroundOffset: function (now)
            {
                this.backgroundOffsetX += this.bgVelocityX * (now - this.lastAnimationFrameTime) / 1000;
                if (this.backgroundOffsetX < 0 || this.backgroundOffsetX > this.canvas.width)
                {
                    this.backgroundOffsetX = 0;
                }

                this.backgroundOffsetY += this.bgVelocityY * (now - this.lastAnimationFrameTime) / 1000;
                if (this.backgroundOffsetY < -(880 - 400))
                {
                    this.backgroundOffsetY = -(880 - 400);
                    this.bgVelocityY = 0;
                }
                else if(this.backgroundOffsetY > 0)
                {
                    this.backgroundOffsetY = 0;
                    this.bgVelocityY = 0;
                }
            },
      setOffsets: function (now)
      {
          this.setBackgroundOffset(now);
          this.setPlayerOffset(now);
          this.setSpriteOffset(now);
      },
      setPlayerOffset: function (now)
      {
          this.playerOffsetY += this.pVelocityY * (now - this.lastAnimationFrameTime) / 1000;
          console.log(this.canvasWidth);
          if (this.playerOffsetY > 400 - this.hero.height)
          {
              this.playerOffsetY = 400 - this.hero.height;
          }
          else if (this.playerOffsetY < this.hero.height)
          {
              this.playerOffsetY = this.hero.height;
          }

          this.playerOffsetX += this.pVelocityX * (now - this.lastAnimationFrameTime) / 1000;
          if (this.playerOffsetX > 240 / 2 - this.hero.width/2)
          {
              this.playerOffsetX = 240 / 2 - this.hero.width/2;
          }
          else if (this.playerOffsetX < -240 / 2)
          {
              this.playerOffsetX = -240 / 2;
          }

          this.hero.OffsetX = this.playerOffsetX;
          this.hero.OffsetY = this.playerOffsetY;
      },
      setSpriteOffset: function (now)
      {
          var sprite;
          this.spriteOffsetY += this.bgVelocityY * (now - this.lastAnimationFrameTime) / 1000;
          this.spriteOffsetX += this.bgVelocityX * (now - this.lastAnimationFrameTime) / 1000;


          for (var i = 0; i < this.sprites.length; ++i)
          {
              sprite = this.sprites[i];
              if ('heroSprite' !== sprite.type)
              {
                  sprite.OffsetX = this.spriteOffsetX;
                  sprite.OffsetY = this.spriteOffsetY;
              }
          }
      },
      leftPress: function ()
      {
          this.pVelocityX = this.PLAYER_VELOCITY*this.scrollMultiplier;;
          this.hero.runAnimationRate = this.RUN_ANIMATION_RATE;
          this.hero.artist.cells = this.heroCellsLeftWalk;
      },
      leftRelease: function ()
      {
          this.pVelocityX = 0;
          //this.hero.runAnimationRate = 0;
          this.hero.artist.cells = this.heroCellsBackWalk;
      },
      rightPress: function ()
      {
          this.pVelocityX = -this.PLAYER_VELOCITY*this.scrollMultiplier;;
          this.hero.runAnimationRate = this.RUN_ANIMATION_RATE;
          this.hero.artist.cells = this.heroCellsRightWalk;
      },
      rightRelease: function ()
      {
          this.pVelocityX = 0;
          //this.hero.runAnimationRate = 0;
          this.hero.artist.cells = this.heroCellsBackWalk;
      },
      upPress: function ()
      {
          this.pVelocityY = this.PLAYER_VELOCITY*this.scrollMultiplier;;
          this.bgVelocityY = -this.BACKGROUND_VELOCITY*this.scrollMultiplier;
          this.hero.runAnimationRate = this.RUN_ANIMATION_RATE;
          this.hero.artist.cells = this.heroCellsBackWalk;
      },
      upRelease: function ()
      {
          this.pVelocityY = 0;
          this.bgVelocityY = this.STARTING_BACKGROUND_VELOCITY_Y*this.scrollMultiplier;
          //this.hero.runAnimationRate = 0;
          //this.hero.artist.cells = this.heroCellsBack;
      },
      downPress: function ()
      {
          this.pVelocityY = -this.PLAYER_VELOCITY*this.scrollMultiplier;;
          this.bgVelocityY = 0;
          this.hero.runAnimationRate = this.RUN_ANIMATION_RATE;
          this.hero.artist.cells = this.heroCellsFrontWalk;
      },
      downRelease: function ()
      {
          this.pVelocityY = 0;
          this.bgVelocityY = this.STARTING_BACKGROUND_VELOCITY_Y*this.scrollMultiplier;
          //this.hero.runAnimationRate = 0;
          this.hero.artist.cells = this.heroCellsBackWalk;
      },
      toastAway: function ()
      {
          var TOAST_BETWEEN_STEP = 600;
          this.elementFadeOut(this.toastElement, TOAST_BETWEEN_STEP);
      },
      commenceToast: function (text, duration)
      {
          this.toastElement.innerHTML = text;
          this.elementFadeIn(this.toastElement);
      },
      gameReveal: function ()
      {
          var dimming_controls = 4000;

          this.dimTopBar();
          this.displayCanvas();
          this.dimBottomBar();

          setTimeout(function ()
          {
              Game.dimActions();
              Game.revealTopBar();
          }, dimming_controls);
      },
      dimTopBar: function ()
      {
          var DIM_OPACITY = 0.4;

          this.scoreElement.style.display = 'block';
          this.fpsElement.style.display = 'block';
          this.livesElement.style.display = 'block';
          this.levelElement.style.display = 'block';
          this.livesIDElement.style.display = 'block';

          setTimeout(function ()
          {
              Game.scoreElement.style.opacity = DIM_OPACITY;
              Game.fpsElement.style.opacity = DIM_OPACITY;
              Game.livesElement.style.opacity = DIM_OPACITY;
              Game.levelElement.style.opacity = DIM_OPACITY;
              Game.livesIDElement.style.opacity = DIM_OPACITY;
          }, this.MINI_DELAY);
      },
      displayCanvas: function ()
      {
          this.elementFadeIn(this.canvas);
      },
      revealTopBar: function ()
      {
          this.elementFadeIn(this.livesElement, this.fpsElement, this.livesIDElement, this.levelElement, this.scoreElement);
      },
      dimBottomBar: function ()
      {
          this.elementFadeIn(this.soundAndMusicElement, this.instructionsElement, this.copyrightElement);
      },
      dimActions: function ()
      {
          GAME_OPACITY = 0.6;
          Game.instructionsElement.style.opacity = GAME_OPACITY;
          Game.soundAndMusicElement.style.opacity = GAME_OPACITY;
      },
      elementFadeIn: function ()
      {
          var argument = arguments;
          for (var i = 0; i < argument.length; ++i)
          {
              console.log(argument[i]);
              argument[i].style.display = 'block';
          }

          setTimeout(function () {
              for (var i = 0; i < argument.length; ++i)
              {
                  argument[i].style.opacity = Game.OPAQUE;
              }
          }, this.MINI_DELAY);
      },
      elementFadeOut: function ()
      {
          var argument = arguments,
                  fadeDuration = argument[argument.length - 1];
          for (var i = 0; i < argument.length - 1; ++i)
          {
              console.log(argument[i]);
              argument[i].style.opacity = Game.TRANSPARENT;
          }

          setTimeout(function () {
              for (var i = 0; i < argument.length - 1; ++i)
              {
                  argument[i].style.display = 'none';
              }
          }, fadeDuration);
      },
      pauseSwitch: function ()
      {
          var now = this.timeSystem.calculateGameTime();
          this.paused = !this.paused;
          if (this.paused)
          {
              this.pausedTime = now;
              Game.musicElement.pause();
          }
          else
          {
              this.lastAnimationFrameTime += (now - this.pausedTime);
              Game.musicElement.play();
          }
      },
      
      detectMobile: function()
   {
      Game.mobile = 'ontouchstart' in window;
   },


   initializeContextForMobileInstructions: function () {
      this.context.textAlign = 'center';
      this.context.textBaseline = 'middle';

      this.context.font = '26px fantasy';

      this.context.shadowBlur = 2;
      this.context.shadowOffsetX = 2;
      this.context.shadowOffsetY = 2;
      this.context.shadowColor = 'rgb(0,0,0)';

      this.context.fillStyle = 'yellow';
      this.context.strokeStyle = 'yellow';
   },

   drawMobileDivider: function (cw, ch) {
      this.context.beginPath();
      this.context.moveTo(cw/2, 0);
      this.context.lineTo(cw/2, ch);
      this.context.stroke();
   },

   drawMobileInstructionsLeft: function (cw, ch, topLineOffset, lineHeight) {
      this.context.font = '32px fantasy';
      this.context.fillText('Tap on this side to:', cw/4, ch/2 - topLineOffset);
      this.context.fillStyle = 'white';
      this.context.font = 'italic 26px fantasy';
      this.context.fillText('Turn around when running right', cw/4, ch/2 - topLineOffset + 2*lineHeight);
      this.context.fillText('Jump when running left', cw/4, ch/2 - topLineOffset + 3*lineHeight);
   },

   drawMobileInstructionsRight: function (cw, ch, topLineOffset, lineHeight) {
      this.context.font = '32px fantasy';
      this.context.fillStyle = 'yellow';
      this.context.fillText('Tap on this side to:', 3*cw/4, ch/2 - topLineOffset);
      this.context.fillStyle = 'white';
      this.context.font = 'italic 26px fantasy';
      this.context.fillText('Turn around when running left', 3*cw/4, ch/2 - topLineOffset + 2*lineHeight);
      this.context.fillText('Jump when running right', 3*cw/4, ch/2 - topLineOffset + 3*lineHeight);
      this.context.fillText('Start running', 3*cw/4, ch/2 - topLineOffset + 5*lineHeight);
   },

   drawMobileInstructions: function () {
      var cw = this.canvas.width,
          ch = this.canvas.height,
          TOP_LINE_OFFSET = 115,
          LINE_HEIGHT = 40;

      this.context.save();
      this.initializeContextForMobileInstructions();
      this.drawMobileDivider(cw, ch);
      this.drawMobileInstructionsLeft(cw, ch, TOP_LINE_OFFSET,  LINE_HEIGHT);
      this.drawMobileInstructionsRight(cw, ch, TOP_LINE_OFFSET,  LINE_HEIGHT);
      this.context.restore();
   },

revealMobileStartToast: function () {
      Game.fadeInElements(Game.mobileStartToast);
   },



addTouchEventHandlers: function () {
      Game.canvas.addEventListener(
         'touchstart', 
         Game.touchStart
      );

      Game.canvas.addEventListener(
         'touchend', 
         Game.touchEnd
      );
   },
 touchStart: function (e) {
      if (Game.playing) 
      {
         // Prevent players from inadvertently dragging the game canvas
         e.preventDefault(); 
         var x = e.changedTouches[0].pageX;
         if (Game.playing) 
         {
            if (x < Game.canvas.width/2) 
            {
                Game.leftPress();
            }
            else if (x > Game.canvas.width/2) 
            {
                Game.rightPress();
            }
          }
      }
    },

   touchEnd: function (e) {
      var x = e.changedTouches[0].pageX;
      if (Game.playing) 
         {
            if (x < Game.canvas.width/2) 
            {
                Game.leftRelease();
            }
            else if (x > Game.canvas.width/2) 
            {
                Game.rightRelease();
            }
          }
         // Prevent players from double
         // tapping to zoom into the canvas
         e.preventDefault(); 
      },

   fitScreen: function()
   {
      var arenaSize = Game.calculateArenaSize(Game.getViewportSize());
      Game.resizeElementsToFitScreen(arenaSize.width, arenaSize.height);
   },

   getViewportSize: function()
   {
      return { width: Math.max(document.documentElement.clientWidth   || window.innerWidth || 0), height: Math.max(document.documentElement.clientHeight  || window.innerHeight || 0) };
   },

   calculateArenaSize: function(viewportSize)
   {
      var DESKTOP_ARENA_WIDTH = 240,
          DESKTOP_ARENA_HEIGHT = 400,
          arenaHeight,
          arenaWidth;
      //maintain aspect ratio
      arenaHeight = viewportSize.width*(DESKTOP_ARENA_HEIGHT/DESKTOP_ARENA_WIDTH);
      if(arenaHeight < viewportSize.height) //Height fits
      {
         arenaWidth = viewportSize.width;
      }
      else //Height does not fit
      {
         arenaHeight = viewportSize.height;
         arenaWidth = arenaHeight*(DESKTOP_ARENA_WIDTH/DESKTOP_ARENA_HEIGHT);
      }
      if(arenaWidth > DESKTOP_ARENA_WIDTH)
      {
         arenaWidth = DESKTOP_ARENA_WIDTH;
      }
      if(arenaHeight > DESKTOP_ARENA_HEIGHT)
      {
         arenaHeight = DESKTOP_ARENA_HEIGHT;
      }
      return{ width: arenaWidth, height: arenaHeight};
   },

   resizeElementsToFitScreen: function(arenaWidth, arenaHeight)
   {
       Game.resizeElement(document.getElementById('arena'), arenaWidth, arenaHeight);
       Game.resizeElement(Game.mobileWelcomeToast, arenaWidth, arenaHeight);
       Game.resizeElement(Game.mobileStartToast, arenaWidth, arenaHeight);
   },

    resizeElement: function(element, w, h)
    {
        element.style.width = w +"px";
        element.style.height = h +"px";
    }

  };

//Key codes for movement
var KEY_LEFT_ARROW = 37,
        KEY_RIGHT_ARROW = 39,
        KEY_UP_ARROW = 38,
        KEY_DOWN_ARROW = 40,
        KEY_SPACE = 32,
        KEY_P = 80;
var leftPressed = false,
        rightPressed = false,
        upPressed = false,
        downPressed = false;

//Event listener
//For website version. Mobile version to be added later
window.addEventListener('keydown', function (e) {
    var key = e.keyCode;
    if (key === KEY_LEFT_ARROW)
    {
        leftPressed = true;
        if (!rightPressed)
            Game.leftPress();
    }
    else if (key === KEY_RIGHT_ARROW)
    {
        rightPressed = true;
        if (!leftPressed)
            Game.rightPress();
    }
    else if (key === KEY_UP_ARROW)
    {
        upPressed = true;
        if (!downPressed)
            Game.upPress();
    }
    else if (key === KEY_DOWN_ARROW)
    {
        downPressed = true;
        if (!upPressed)
            Game.downPress();
    }
    else if (key === KEY_P)
    {
        Game.pauseSwitch();
    }
});

window.addEventListener('keyup', function (e) {
    var key = e.keyCode;
    if (key === KEY_LEFT_ARROW)
    {
        Game.leftRelease();
        leftPressed = false;
    }
    else if (key === KEY_RIGHT_ARROW)
    {
        Game.rightRelease();
        rightPressed = false;
    }
    else if (key === KEY_UP_ARROW)
    {
        Game.upRelease();
        upPressed = false;
    }
    else if (key === KEY_DOWN_ARROW)
    {
        Game.downRelease();
        downPressed = false;
    }
});

//Event listener for Focus and blur events

window.addEventListener('blur', function (e) {
    Game.focusWindow = false;
    if (!Game.paused)
    {
      Game.pauseSwitch();
      Game.musicElement.pause();
    }
});

window.addEventListener('focus', function (e) {
    var startingFont = Game.toastElement.style.fontSize,
            COUNTDOWN_DISPLAY = 1000;
    Game.focusWindow = true;
    Game.currentCountdown = true;
    if (Game.paused)
    {
        Game.toastElement.style.font = '100px Triforce';
        if (Game.focusWindow && Game.currentCountdown)
        {
            Game.toastDisplay('3', COUNTDOWN_DISPLAY);
        }
        setTimeout(function (e)
        {
            if (Game.focusWindow && Game.currentCountdown)
                Game.toastDisplay('2', COUNTDOWN_DISPLAY);

            setTimeout(function (e)
            {
                if (Game.focusWindow && Game.currentCountdown)
                    Game.toastDisplay('1', COUNTDOWN_DISPLAY);

                setTimeout(function (e)
                {
                    if (Game.focusWindow && Game.currentCountdown)
                    {
                        Game.pauseSwitch();
                        Game.musicElement.play();
                    }
                    if (Game.focusWindow && Game.currentCountdown)
                    {
                        Game.toastElement.style.fontSize = startingFont;
                    }
                    Game.currentCountdown = false;
                }, COUNTDOWN_DISPLAY);
            }, COUNTDOWN_DISPLAY);
        }, COUNTDOWN_DISPLAY);
    }
});

var Game = new Game();

Game.musicCheckboxElement.addEventListener('change', function (e){
   Game.musicOn = Game.musicCheckboxElement.checked;
   if(Game.musicOn)
   {
      Game.musicElement.play();
   }
   else
   {
      Game.musicElement.pause();
   }
});

Game.soundCheckboxElement.addEventListener('change', function (e){
   Game.soundOn = Game.soundCheckboxElement.checked;
});

Game.welcomeStartLink.addEventListener('click',
   function(e)
   {
      var FADE_DURATION = 1000;
      Game.elementFadeOut(Game.mobileWelcomeToast, FADE_DURATION);
      Game.playing = true;
   });

Game.mobileStartLink.addEventListener('click',
   function(e)
   {
      var FADE_DURATION = 1000;
      Game.elementFadeOut(Game.mobileStartToast, FADE_DURATION);
      Game.mobileInstructionsVisible = false;
      Game.playing = true;
   });

Game.showHowLink.addEventListener('click',
   function(e)
   {
      var FADE_DURATION = 1000;
      Game.fadeOutElements(Game.mobileWelcomeToast, FADE_DURATION);
      Game.drawMobileInstructions();
      Game.revealMobileStartToast();
      Game.mobileInstructionsVisible = true;
   });

Game.imagesInit();
Game.createSprites();
Game.createAudioChannels();
Game.detectMobile();

if(Game.mobile)
{
   //mobile specific stuff - touch handler etc
   Game.instructionsElement = document.getElementById('game-mobile-instructions');
   Game.addTouchEventHandlers();

    if (/android/i.test(navigator.userAgent)) 
    {
      Game.explosionSound.position = 2.8;
      Game.pianoSound.position = 3.5;
   }
}


Game.fitScreen();
window.addEventListener("resize", Game.fitScreen());
window.addEventListener("orientationchange", Game.fitScreen());